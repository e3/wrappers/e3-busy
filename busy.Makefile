where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile



APP:=busyApp
APPDB:=$(APP)/Db
APPSRC:=$(APP)/src

USR_INCLUDES += -I$(where_am_I)$(APPSRC)

TEMPLATES += $(APPDB)/busyRecord.db
TEMPLATES += $(APPDB)/testBusyAsyn.db
TEMPLATES += $(wildcard $(APPDB)/*.req)


DBDINC_SRCS = $(APPSRC)/busyRecord.c
DBDINC_DBDS = $(subst .c,.dbd,   $(DBDINC_SRCS:$(APPSRC)/%=%))
DBDINC_HDRS = $(subst .c,.h,     $(DBDINC_SRCS:$(APPSRC)/%=%))
DBDINC_DEPS = $(subst .c,$(DEP), $(DBDINC_SRCS:$(APPSRC)/%=%))



# Community has the following DBD files
# busySupport.dbd and busyRecord.dbd
# They are actually the same as busy.dbd
# Anyway, we have to tell driver.makefile
# the following dbd files, but we don't need to
# tell driver.makefile where busyRecord.dbd is


SOURCES   += $(APPSRC)/devBusyAsyn.c

SOURCES   += $(APPSRC)/devBusySoftRaw.c
SOURCES   += $(APPSRC)/devBusySoft.c


DBDS      += $(APPSRC)/busySupport_LOCAL.dbd
DBDS      += $(APPSRC)/busySupport_withASYN.dbd


HEADERS += $(DBDINC_HDRS)
SOURCES += $(DBDINC_SRCS)


.PHONY: $(DBDINC_DEPS)
$(DBDINC_DEPS): $(DBDINC_HDRS)

.PHONY: .dbd.h
.dbd.h:
	$(DBTORECORDTYPEH)  $(USR_DBDFLAGS) -o $@ $<


.PHONY: vlibs
vlibs:
